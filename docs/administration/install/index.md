# Install

Mobilizon can be installed through different methods:

* [install a (precompiled) release](./release.md) (recommended)
* [run a Docker image](./docker.md) (standalone or with `docker-compose`)
* [install from source](./source.md)

