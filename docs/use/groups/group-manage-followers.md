# Manage followers through the federation

## Management page

As a group administrator, you can manage [the users who subscribe to your group's feed through the federation](../../users/follow-federation/) by clicking on:

  1. the **My Groups** tab on the top navigation bar
  1. your group name
  1. **Add / Remove…** (in the banner of your group)
  1. the **Followers** tab (left menu)

## Reject a follower subscribe

To reject a subscription, you must, from the management page (see above) click on the **Reject** button in front of the subscription you want.
