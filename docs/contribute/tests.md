# Tests

## Backend

The backend uses `ExUnit`.

To launch all the tests:
```bash
mix test
```

!!! info
    If you're using Docker, you can use `make test`

If you want test coverage:

```bash
mix coveralls.html
```

It will show the coverage and will output a `cover/excoveralls.html` file.

If you want to test a single file:

```bash
mix test test/mobilizon/actors/actors_test.exs
```

If you want to test a specific test, block or line:

```bash
mix test test/mobilizon/actors/actors_test.exs:85
```

!!! tip
    Note: The `coveralls.html` command also works the same

## Front-end

### Unit tests

We use [Vue Test Utils](https://vue-test-utils.vuejs.org/) and [Jest](https://jestjs.io/) to run unit tests.

When inside the `js` directory, run the tests with
```bash
yarn run test:unit
```

Jest set of [CLI options](https://jestjs.io/docs/cli) can be used here.
